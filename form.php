<?php

function generateSelect($name, $options, $selected = NULL, $specials = TRUE) {
	$str = '';
	$str .= '<select name="'.$name.'">';
	if ($specials == TRUE) {
		$xoptions = array('x-axis', 'all');
		$str .= '<optgroup label="---">';
		foreach ($xoptions as $o) {
			if ($selected && ($selected == $o)) {
				$str .= '<option value="'.$o.'" selected>'.$o.'</option>';
			} else {
				$str .= '<option value="'.$o.'">'.$o.'</option>';
			}
		}
		$str .= '</optgroup>';
	}
	if (count($options)) {
		if ($specials == TRUE) {
			$str .= '<optgroup label="---">';
		}
		foreach ($options as $o) {
			if ($selected && ($selected == $o)) {
				$str .= '<option value="'.$o.'" selected>'.$o.'</option>';
			} else {
				$str .= '<option value="'.$o.'">'.$o.'</option>';
			}
		}
		if ($specials == TRUE) {
			$str .= '</optgroup>';
		}
	}
	$str .= '</select>';

	return $str;
}

function generateCheckbox($name, $selected = NULL) {
	$str = '';
	$str .= '<input type=checkbox name="'.$name.'" ';
	if ($selected && ($selected == TRUE)) {
		$str .= 'value="'.$name.'" checked>';
	} else {
		$str .= 'value="'.$name.'">';
	}
	$str .= '</input>';

	return $str;
}

function getForm($data) {

	// what is the latest plot index?
	$plotIdx = 1;
	if (isset($_POST['PLOTIDX'])) {
		$plotIdx = $_POST['PLOTIDX'];
	}
	//echo "Latest plot index $plotIdx<br>";
	if (isset($_POST['ACTION'])) {
		if (strncmp($_POST['ACTION'], 'REM-', 4) === 0) {
			// we have a remove plot action
			sscanf($_POST['ACTION'], 'REM-%d', $rem);
			//echo "Removing plot at index $rem<br>";
			// do not include this index in the new form
			unset($_POST['INDEX-'.$rem]);
		} elseif (strncmp($_POST['ACTION'], 'Add', 3) === 0) {
			// we have an add plot action
			//echo "Adding a plot..<br>";
			$_POST['INDEX-'.$plotIdx] = $plotIdx;
			$plotIdx++;
			//echo "New plot index $plotIdx<br>";
 		}
	}

	$test_cases = $data->getCachedItems('testcase');
	$test_dtype1 = $data->getCachedItems('dtype1');
	$test_dtype2 = $data->getCachedItems('dtype2');
	$test_opt = $data->getCachedItems('opt');
	$test_loops = $data->getCachedItems('loops');
	$test_samples = $data->getCachedItems('samples');
	$test_hosts = $data->getCachedItems('hostname');
	$test_case_filter = '';
	if (isset($_POST['testcase-exclude'])) {
		$test_case_filter = $_POST['testcase-exclude'];
	}


	$str = '';
	$str .= '<table border=1>';
	// separate the file upload form..
	$str .= '<form action=index.php method=post enctype="multipart/form-data">';
	$str .= '<tr>';
	$str .= '<td>';
	$str .= 'Upload new data set';
	$str .= '</td>';
	$str .= '<td>';
	$str .= '<input type=file name=CSVFILE></input>';
	$str .= '</td>';
	$str .= '<td>';
	$str .= '<input type=submit name=ACTION value=Upload></input>';
	$str .= '</td>';
	$str .= '</tr>';
	$str .= '</form>';

	// separate the data set remove form..
	$str .= '<form action=index.php method=post>';
	$str .= '<tr>';
	$str .= '<td>';
	$str .= 'Remove data set';
	$str .= '</td>';
	$str .= '<td>';
	$str .= generateSelect('hostname',
			$test_hosts,
			NULL,
			FALSE);
	$str .= '</td>';
	$str .= '<td>';
	$str .= '<input type=submit name=ACTION value=Remove></input>';
	$str .= '</td>';
	$str .= '</tr>';
	$str .= '</form>';
	$str .= '</table>';

	$str .= '<br>';

	$str .= '<form action=index.php method=post>';
	$str .= '<table border=1>';
	$str .= '<tr>';
	$str .= '<th>action</th><th>host</th><th>test case</th><th>samples</th><th>loops</th><th>optimization</th><th>data type 1</th><th>data type 2</th>';
	$str .= '<th>average</th><th>max</th><th>total</th>';
	$str .= '</tr>';
	for ($i = 1; $i <= $plotIdx; $i++) {

		if (! isset($_POST['INDEX-'.$i])) {
			//echo 'Plot index '.$i. ' is not in use..<br>';
			continue;
		}

		$str .= '<tr>';

		$str .= '<td>';
		$str .= '<input type=submit name=ACTION value=REM-'.$i.'></input>';
		$str .= '<input type=hidden name=INDEX-'.$i.' value='.$i.'></input>';
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('hostname-'.$i,
				$test_hosts,
				(isset($_POST['hostname-'.$i]) ? $_POST['hostname-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('testcase-'.$i,
				$test_cases,
				(isset($_POST['testcase-'.$i]) ? $_POST['testcase-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('samples-'.$i,
				$test_samples,
				(isset($_POST['samples-'.$i]) ? $_POST['samples-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('loops-'.$i,
				$test_loops,
				(isset($_POST['loops-'.$i]) ? $_POST['loops-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('opt-'.$i,
				$test_opt,
				(isset($_POST['opt-'.$i]) ? $_POST['opt-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('dtype1-'.$i,
				$test_dtype1,
				(isset($_POST['dtype1-'.$i]) ? $_POST['dtype1-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateSelect('dtype2-'.$i,
				$test_dtype2,
				(isset($_POST['dtype2-'.$i]) ? $_POST['dtype2-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateCheckbox('average-'.$i,
				(isset($_POST['average-'.$i]) ? $_POST['average-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateCheckbox('maximum-'.$i,
				(isset($_POST['maximum-'.$i]) ? $_POST['maximum-'.$i] : NULL));
		$str .= '</td>';

		$str .= '<td>';
		$str .= generateCheckbox('total-'.$i,
				(isset($_POST['total-'.$i]) ? $_POST['total-'.$i] : NULL));
		$str .= '</td>';

		$str .= '</tr>';
	}

	$str .= '<tr>';
	$str .= '<td>';
	$str .= '<input type=submit name=ACTION value=Add></input>';
	$str .= '</td>';
	$str .= '</tr>';
	$str .= '</table>';

	$str .= '<br>';

	$str .= '<table border=1>';
	$str .= '<tr>';
	$str .= '<td>';
	$str .= 'Exclude test cases(s)';
	$str .= '</td>';
	$str .= '<td>';
	$str .= '<input type=text name=testcase-exclude value='.$test_case_filter.'></input>';
	$str .= '</td>';
	$str .= '</tr>';
	$str .= '<tr>';
	$str .= '<td>';
	$str .= 'Plot size [w x h]';
	$str .= '</td>';
	$str .= '<td colspan=4>';
	$str .= '<input type=text name=plot-width value='.$data->getPlotWidth().'></input>';
	$str .= ' x ';
	$str .= '<input type=text name=plot-height value='.$data->getPlotHeight().'></input>';
	$str .= '</td>';
	$str .= '</tr>';
 	$str .= '</table>';

 	$str .= '<br>';

 	$str .= '<table border=1>';
 	$str .= '<tr>';
	$str .= '<td>';
	$str .= '<input type=hidden name=PLOTIDX value='.$plotIdx.'></input>';
	$str .= '<input type=submit name=ACTION value=Refresh></input>';
	$str .= '</td>';
	$str .= '</tr>';
 	$str .= '</table>';

	$str .= '</form>';
	return $str;
}