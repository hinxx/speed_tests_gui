CREATE TABLE IF NOT EXISTS csv (
	hostname STRING,
	testcase STRING,
	dtype1 STRING,
	dtype2 STRING,
	samples INTEGER,
	loops INTEGER,
	average DOUBLE,
	maximum DOUBLE,
	total DOUBLE,
	cpuload INTEGER,
	cpus INTEGER,
	opt STRING,
	op STRING
);

CREATE TABLE IF NOT EXISTS cache (
	key STRING,
	value STRING
);

