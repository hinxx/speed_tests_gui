<?php


function test_case_rename(&$item, $key) {
	$item = preg_replace("/\.c/", '', $item);
}

class csv {
	private $files = array();
	private $delimiter = '|';
	private $data = array();
	private $header = '';
	private $items = array();
	private $xCols = array('HOST', 'CASE', 'DTYPE1', 'DTYPE2', 'OPTFLAGS', 'LOOPS', 'SAMPLES');
	private $walk_fnc = array(NULL, 'test_case_rename', NULL, NULL, NULL, NULL, NULL);
	private $yCols = array('AVG', 'MAX', 'TOTAL');
	private $dataDir = '';

	function __construct($delimiter = '|') {
		$this->delimiter = $delimiter;

		$this->dataDir = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/data';
		if (! is_dir($this->dataDir)) {
			mkdir($this->dataDir);
		}

		// find CSV files
		$csvFiles = $this->findCsvFiles();
		// load data from CSV file(s)
		foreach ($csvFiles as $md5 => $file) {
			$this->loadCsvFile($file);
		}

		// XXX This bit of code takes a lot of time :s
		$c = count($this->xCols);
		for ($i = 0; $i < $c; $i++) {
			$this->items[$this->xCols[$i]] = $this->createItems($this->xCols[$i], $this->walk_fnc[$i]);
		}
	}

	function findCsvFiles() {
// 		echo '<pre>';
		$files = scandir($this->dataDir);
// 		print_r($files);
		$csvFiles = array();
		foreach ($files as $f) {
			if (preg_match("/\.csv$/", $f)) {
				$csvFiles[] = $this->dataDir.'/'.$f;
			}
		}
// 		print_r($csvFiles);
// 		echo '</pre>';

		return $csvFiles;
	}

	function loadCsvFile($filename) {
		if (!$this->csvToArray($filename)) {
			echo 'Failed to get data from CSV file '.$filename.'<br>';
			return FALSE;
		}
		$md5file = md5_file($filename);
		$this->files[$md5file] = $filename;

//		echo 'Loaded data from CSV file '.$filename.'<br>';
		return TRUE;
	}

	function uploadCsvFile($tmpfile, $filename) {
		$stamp = date("D M j G:i:s T Y");
		$md5file = md5_file($tmpfile);
		if (count($this->files) > 0) {
			foreach ($this->files as $md5 => $file) {
				if ($md5 == $md5file) {
					echo 'File with the same MD5 already exists.<br>';
					return FALSE;
				}
			}
		}

		return move_uploaded_file($tmpfile, $this->dataDir.'/'.$stamp.'-'.$filename);
	}

	function getXColsList() {
		return $this->xCols;
	}

	function getNrXCols() {
		return count($this->xCols);
	}

	function getYColsList() {
		return $this->yCols;
	}

	function getNrYCols() {
		return count($this->yCols);
	}

	/**
	 * Convert a comma separated file into an associated array.
	 * The first row should contain the array keys.
	 * @return array
	 * @link http://gist.github.com/385876
	 * @author Jay Williams <http://myd3.com/>
	 * @copyright Copyright (c) 2010, Jay Williams
	 * @license http://www.opensource.org/licenses/mit-license.php MIT License
	 */
	function csvToArray($filename) {
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$this->header = NULL;
		if (($handle = fopen($filename, 'r')) !== FALSE) {
			while (($row = fgetcsv($handle, 1000, $this->delimiter)) !== FALSE) {
				if(!$this->header)
					$this->header = $row;
				else
					$this->data[] = array_combine($this->header, $row);
			}
			fclose($handle);
		}
		return TRUE;
	}

	function getHeader() {
		return $this->header;
	}

	function getRows($filter = array(), $limit = NULL) {
		$rows = array();
		$doAdd = 0;
		$items = 0;

		foreach ($this->data as $dKey => $dValue) {
			$doAdd = 0;
			foreach ($filter as $fKey => $fValue) {
				//echo 'filter: '.$fKey.'=>'.$fValue.'<br>';
				//echo '$dValue[$fKey]: '.$dValue[$fKey].'<br>';
				if ($fKey && $fValue) {
					if ($fValue == $dValue[$fKey]) {
						//$rows[] = $dValue;
						$doAdd++;
					}
				}
			}

			if ($doAdd == count($filter)) {
				$rows[] = $dValue;
				$items++;
				if (is_integer($limit) && ($items == $limit)) {
					return $rows;
				}
			}
		}

		return $rows;
	}

	function getRow($filter = array()) {
		return $this->getRows($filter, 1);
	}

	function getColumn($colName) {
		$found = FALSE;
		foreach ($this->header as $key => $value) {
			if ($value == $colName) {
				$found = TRUE;
			}
		}
		if (! $found) {
			echo 'Column '.$colName.' is NOT present!<br>';
			return NULL;
		}

		$data = array();
		foreach ($this->data as $key => $value) {
			$data[$key] = $value[$colName];
		}
		return $data;
	}

	function createItems($col, $walk_fnc = NULL) {
		$items = $this->getColumn($col);
		$keys = array_values(array_unique(array_values($items), SORT_STRING));
		$values = $keys;
		if ($walk_fnc) {
			array_walk($values, $walk_fnc);
		}
		return array_combine($keys, $values);
	}

	function getItems($col) {
		if (!isset($this->items[$col]))
			return NULL;
		return $this->items[$col];
	}

	function info() {
		$str = '<p>Number of test results: '.count($this->data);
		$str .= '<p>Number of test cases: '.count($this->getItems('CASE'));
		$str .= '<p>Compiler optimization variants:';
		$str .= '<ul>';
		foreach ($this->getItems('OPTFLAGS') as $key => $value) {
			$str .= '<li>'.$value.'</li>';
		}
		$str .= '</ul>';
		return $str;
	}

	function machine() {
		$str = '';
		$str .= 'Data from following machines is present:<br>';
		foreach ($this->getItems('HOST') as $value) {
			$str .= '<div id='.$value.'></div>';
			$str .= '<h4>'.$value.'</h4>';
			$filename = 'machines/'.$value.'.html';
			if (file_exists($filename)) {
				$str .= file_get_contents($filename);
			} else {
				$str .= '<p>Information for the machine '.$value.' is not present';
			}
			$str .= '<br>';
		}
		return $str;
	}

	function knownTestOps() {
		$testCases = $this->getItems('CASE');

		$str = '<table id=testcases  border=1>';
		$str .= '<tr>';
		$str .= '<th>name</th><th>operation(s)</th>';
		$str .= '</tr>';
		foreach ($testCases as $key => $value) {
			$row = $this->getRow(array('CASE' => $key))[0];
			$op = preg_replace(array("/^\{\s/", "/\s\}$/", "/ \}/", "/\{/"),
								array("", "", "\n}", "{\n"),
								$row['OP']);
			$str .= '<tr>';
			$str .= '<td>'.$value.'</td><td><pre>'.$op.'</pre></td>';
			$str .= '</tr>';
		}
		$str .= '</table>';

		return $str;
	}
}

?>