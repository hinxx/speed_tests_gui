<?php
require_once 'phplot-6.1.0/phplot.php';

function plotSingle($idx, $width, $height, $data, $title, $xTitle, $yTitle, $legend) {
	$file = './plots/file-'.$idx.'.png';

	$plot = new PHPlot($width, $height);
	$plot->SetOutputFile($file);
	$plot->SetIsInline(TRUE);
	$plot->SetPrintImage(FALSE);
	$plot->SetPlotType('bars');
	$plot->SetTitle($title);
	$plot->SetXTitle($xTitle);
	$plot->SetYTitle($yTitle);
	$plot->SetDataValues($data);
	$plot->SetLegend($legend);
	//$plot->SetLegendPosition(0, 0, 'image', 0, 0, 5, 5);
	//$plot->SetLegendPosition(1, 0, 'image', 1, 0, -5, 5);
	$plot->DrawGraph();
	$plot->PrintImage();

	//echo 'plot at index '.$idx.' was created as image'.$file.', '.$width.' x '.$height.'<br>';

	return $file;
}

function sortRows($rows, $key) {
	$a = array();
	foreach ($rows as $k => $v) {
// 		echo 'a key '.$v[$key].' = '.$k.'<br>';
		$a[$k] = $v[$key];
	}
	asort($a);
	$r = array();
	foreach ($a as $k => $v) {
// 		echo 'r key '.$k.' = '.$v.'<br>';
		$r[] = $rows[$k];
	}
	return $r;
}

function plotData($data) {
	// what is the latest plot index?
	$plotIdx = 1;
	if (isset($_POST['PLOTIDX'])) {
		$plotIdx = $_POST['PLOTIDX'];
	}

	$plotCount = 0;

	$str = '';
	$str .= '<table border=1>';
	for ($i = 1; $i <= $plotIdx; $i++) {

		if (! isset($_POST['INDEX-'.$i])) {
			//echo 'Plot index '.$i. ' is not in use..<br>';
			continue;
		}

// 		echo '<pre>';
// 		echo 'Handling data for index '.$i.'<br>';
// 		print_r($data);
// 		echo '</pre>';

		$filter = array();
		$xCols = $data->getXColsList();
		$nrXCols = count($xCols);
		$haveXAxis = array();
		$haveAll = FALSE;
		for ($c = 0; $c < $nrXCols; $c++) {
			$idx = $xCols[$c];
			$postIdx = $idx.'-'.$i;
			if (isset($_POST[$postIdx])) {
				//echo 'Handling postIdx '.$postIdx.'<br>';
				if (($_POST[$postIdx] != 'all')) {
					if ($_POST[$postIdx] != 'x-axis') {
						$filter[$idx] = $_POST[$postIdx];
					} else {
						array_push($haveXAxis, $idx);
					}
				} else {
					$haveAll = $idx;
				}
			}
		}

		// only one X axis is allowed!
		if (count($haveXAxis) == 0) {
			echo '<div id=err><br>Plot '.$i.': X axis not set!<br></div>';
			continue;
		} elseif (count($haveXAxis) > 1) {
			echo '<div id=err><br>Plot '.$i.': Too many X axis set!<br></div>';
			continue;
		}
		$haveXAxis = $haveXAxis[0];

		$yCols = $data->getYColsList();
		$nrYCols = count($yCols);
		$haveYAxis = array();
		for ($c = 0; $c < $nrYCols; $c++) {
			$idx = $yCols[$c];
			$postIdx = $idx.'-'.$i;
			if (isset($_POST[$postIdx])) {
				array_push($haveYAxis, $idx);
			}
		}

		// one or more Y axis is allowed if 'all' is not set
		if (count($haveYAxis) == 0) {
			echo '<div id=err><br>Plot '.$i.': Y axis not set!<br></div>';
			continue;
		} elseif ((count($haveYAxis) > 1) && ($haveAll != FALSE)) {
			echo '<div id=err><br>Plot '.$i.': Too many Y axis set!<br></div>';
			continue;
		}

// 		echo 'These are filters for index '.$i.'<br>';
// 		print_r($filter);
// 		echo '<br>This is X axis index '.$i.' = '.$haveXAxis.'<br>';
// 		echo '<br>This is Y axis index '.$i.'<br>';
// 		print_r($haveYAxis);
// 		echo '<br>This is have all index '.$i.' = '.$haveAll.'<br>';

		$rows = $data->getRows($filter);
// 		echo '<pre>';
// 		echo '<br>rows unsorted<br>';
		$plotData = array();
// 		print_r($rows);
//		$rows = sortRows($rows, $haveXAxis);
// 		echo '<br>sorted<br>';
// 		print_r($rows);
//		echo '</pre>';

		if ($haveAll) {
			$allItems = $data->getItems($haveAll);
			if (count($allItems) == 0) {
				return '<div id=err><br>No CSV data files loaded.<br></div>';
			}
// 			echo '<pre>All items<br>';
// 			print_r($allItems);
// 			echo '</pre>';
			$allRows = array();
			foreach ($allItems as $item) {
				$skip = FALSE;
				if (isset($_POST['testcase-exclude'])) {
					$exclude = explode(',', $_POST['testcase-exclude']);
					foreach ($exclude as $ex) {
						if ($item == $ex) {
							$skip = TRUE;
						}
					}
				}
				if ($skip == TRUE) {
					continue;
				}
				foreach ($rows as $row) {
					if ($row[$haveAll] == $item) {
						$allRows[$item][] = $row;
					}
				}
			}
// 			echo '<pre>All rows<br>';
// 			print_r($allRows);
// 			echo '</pre>';

			// legend is an array of selected data sets
			$legend = array();

			foreach ($allRows as $rowKey => $rowValue) {
				$d = array();
				array_push($d, $rowKey);
				foreach ($rowValue as $row) {
					$skip = FALSE;
					if (isset($_POST['testcase-exclude'])) {
						$exclude = explode(',', $_POST['testcase-exclude']);
						foreach ($exclude as $ex) {
							if ($row[$haveXAxis] == $ex) {
								$skip = TRUE;
							}
						}
					}
					if ($skip == TRUE) {
						continue;
					}
					array_push($d, $row[$haveYAxis[0]]);
					$legend[$row[$haveXAxis]] = $row[$haveXAxis];
				}
				array_push($plotData, $d);
			}
			$xTitle = $haveAll;
		} else {
			foreach ($rows as $row) {
				$d = array();

				array_push($d, $row[$haveXAxis]);
				// several data sets may be plotted on the same plot
				foreach ($haveYAxis as $a) {
					array_push($d, $row[$a]);
				}
				array_push($plotData, $d);
			}

			// legend is an array of selected data sets
			$legend = $haveYAxis;
			$xTitle = $haveXAxis;
		}

		$title = implode(', ', $filter);
		$yTitle = 'Time [ms]';
		$file = plotSingle($i, $data->getPlotWidth(), $data->getPlotHeight(), $plotData, $title, $xTitle, $yTitle, $legend);

		// two plots in a row
		if (($plotCount % 2) == 0) {
	 		$str .= '<tr>';
		}
		$str .= '<td>';
		$str .= '<img src="'.$file.'">';
		$str .= '</td>';
		if (($plotCount % 2) == 1) {
			$str .= '</tr>';
		}
		$plotCount++;
	}
	// close the row if needed
	if (($plotCount % 2) == 1) {
		$str .= '</tr>';
	}
	$str .= '</table>';

	if ($plotCount) {
		return $str;
	} else {
		return '';
	}
}


