<?php

class data {
	private $db = NULL;
	private $dataDir = '';
	private $dataFile = '';
	private $xCols = array('hostname', 'testcase', 'dtype1', 'dtype2', 'opt', 'loops', 'samples');
	private $yCols = array('average', 'maximum', 'total');
	private $plotWidth;
	private $plotHeight;

	function __construct() {
		$this->dataDir = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/data';
		if (! is_dir($this->dataDir)) {
			mkdir($this->dataDir);
		}
		$this->dataFile = $this->dataDir.'/data.db';
		$this->db = new SQLite3($this->dataFile);
		$this->plotWidth = 600;
		$this->plotHeight = 600;
	}

	function csvToDb($filename) {
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$delimiter = '|';
		$doCheck = TRUE;
		if (($handle = fopen($filename, 'r')) !== FALSE) {
			// skip header
			$row = fgetcsv($handle, 1000, $delimiter);
			$nrItems = count($row) - 1;
			$sql1 = 'INSERT INTO csv (
						hostname,
						testcase,
						dtype1,
						dtype2,
						samples,
						loops,
						average,
						maximum,
						total,
						cpuload,
						cpus,
						opt,
						op
						) VALUES ';
			$sql = '';
			// get all data lines
			while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
				$sql .= $sql1 . '(';
				array_shift($row);
				// see if this is a duplicate host
				if ($doCheck) {
					foreach ($this->getItems('hostname') as $value) {
						if ($value == $row[0]) {
							echo '<div id=err><br>Refusing to add data set of exisitng host '.$value.'.<br></div>';
							return TRUE;
						}
					}
					$doCheck = FALSE;
				}
				// get every column into the SQL statement
				foreach ($row as $k => $v) {
					$sql .= "'".$v."'";
					if ($k != $nrItems - 1) {
						$sql .= ', ';
					}
				}
				$sql .= ');';
			}
			// wrap the INSERTs into transaction to speed things up
			$sql = 'BEGIN TRANSACTION;' . $sql . 'END TRANSACTION;';
			//echo 'SQL '.$sql;
			// do this only once!
			$this->db->exec($sql);
			fclose($handle);

			// update cache values
			$this->updateCache();
		}

		return TRUE;
	}

	function clearCache() {
		$sql = 'DELETE FROM cache;';
		//echo "SQL: $sql";
		$this->db->exec($sql);
	}

	function updateCache() {
		$this->clearCache();
		$sql1 = 'INSERT INTO cache (
						key,
						value
						) VALUES ';
		$sql = '';
		// go over all the columns that need to be cached
		foreach ($this->xCols as $col) {
			$rows = $this->getItems($col);
			foreach ($rows as $k => $v) {
				$sql .= $sql1 . '(';
				$sql .= "'".$col."', "."'".$v."'";
				$sql .= ');';
			}
		}

		// wrap the INSERTs into transaction to speed things up
		$sql = 'BEGIN TRANSACTION;' . $sql . 'END TRANSACTION;';
		//echo 'SQL '.$sql;
		// do this only once!
		$this->db->exec($sql);
	}

	function getCachedItems($col) {
		$sql = 'SELECT * FROM cache WHERE key = "'.$col.'";';
		//echo 'SQL '.$sql.':<br>';
		$ret = $this->db->query($sql);
		if (! $ret)
			return array();
		$data = array();
		while ($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
			$data[] = $row['value'];
		}
		//echo 'Cached data for col '.$col.':<br>';
		//print_r($data);
		return $data;
	}

	function uploadCsvFile($tmpfile, $filename) {
		$stamp = date("D M j G:i:s T Y");
		$file = $this->dataDir.'/'.$stamp.'-'.$filename;
		$ret = move_uploaded_file($tmpfile, $file);
		if ($ret == TRUE) {
			$this->csvToDb($file);
		}
	}

	function getItems($col) {
		$sql = 'SELECT DISTINCT '.$col.' FROM csv';
		$ret = $this->db->query($sql);
		if (! $ret)
			return array();
		$data = array();
		while ($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
			$data[] = $row[$col];
		}
		return $data;
	}

	function getXColsList() {
		return $this->xCols;
	}

	function getNrXCols() {
		return count($this->xCols);
	}

	function getYColsList() {
		return $this->yCols;
	}

	function getNrYCols() {
		return count($this->yCols);
	}

	function getPlotHeight() {
		return $this->plotHeight;
	}

	function setPlotHeight($height) {
		$this->plotHeight = $height;
	}

	function getPlotWidth() {
		return $this->plotWidth;
	}

	function setPlotWidth($width) {
		$this->plotWidth = $width;
	}

	function getRows($filter = array(), $limit = NULL) {
		$sql = 'SELECT * FROM csv ';
		if (count($filter) > 0) {
			$sql .= 'WHERE ';
			$nr = count($filter);
			$i = 0;
			foreach ($filter as $key => $value) {
				$sql .= $key.' = "'.$value.'"';
				if ($i != ($nr - 1)) {
					$sql .= ' AND ';
				}
				$i++;
			}
			if ($limit) {
				$sql .= ' LIMIT '.$limit;
			}
		} else {
			if ($limit) {
				$sql .= ' LIMIT '.$limit;
			}
			$sql .= ';';
		}

		//echo "SQL: $sql";
		$ret = $this->db->query($sql);
		if (! $ret)
			return array();
		$data = array();
		while ($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
			$data[] = $row;
		}
		return $data;
	}

	function getRow($filter = array()) {
		return $this->getRows($filter, 1);
	}

	function __destruct() {
		$this->db->close();
	}


	function maxRowID() {
		$sql = 'SELECT MAX(ROWID) FROM csv;';

		//echo "SQL: $sql";
		$ret = $this->db->querySingle($sql);
		if (! $ret)
			return -1;
		return $ret;
	}

	function info() {
		$str = '';
 		$str .= '<p>Number of test results: '.$this->maxRowID();
		$str .= '<p>Number of test cases: '.count($this->getItems('testcase'));
		$str .= '<p>Compiler optimization variants:';
		$str .= '<ul>';
		foreach ($this->getItems('opt') as $value) {
			$str .= '<li>'.$value.'</li>';
		}
		$str .= '</ul>';
		return $str;
	}

	function machine() {
		$str = '';
		$str .= 'Data from following machines is present:<br>';
		foreach ($this->getItems('hostname') as $value) {
			$str .= '<div id='.$value.'></div>';
			$str .= '<h4>'.$value.'</h4>';
			$filename = 'machines/'.$value.'.html';
			if (file_exists($filename)) {
				$str .= file_get_contents($filename);
			} else {
				$str .= '<p>Information for the machine '.$value.' is not present';
			}
			$str .= '<br>';
		}
		return $str;
	}

	function knownTestOps() {
		$testCases = $this->getItems('testcase');
		$str = '<table id=testcases  border=1>';
		$str .= '<tr>';
		$str .= '<th>name</th><th>operation(s)</th>';
		$str .= '</tr>';
		foreach ($testCases as $value) {
			$row = $this->getRow(array('testcase' => $value))[0];
			$op = preg_replace(array("/^\{\s/", "/\s\}$/", "/ \}/", "/\{/"),
								array("", "", "\n}", "{\n"),
								$row['op']);
			$str .= '<tr>';
			$str .= '<td>'.$value.'</td><td><pre>'.$op.'</pre></td>';
			$str .= '</tr>';
		}
		$str .= '</table>';

		return $str;
	}

	function remove($filter) {
		$sql = 'DELETE FROM csv ';
		if (count($filter) > 0) {
			$sql .= 'WHERE ';
			$nr = count($filter);
			$i = 0;
			foreach ($filter as $key => $value) {
				$sql .= $key.' = "'.$value.'"';
				if ($i != ($nr - 1)) {
					$sql .= ' AND ';
				}
				$i++;
			}
		} else {
			$sql .= ';';
		}

		//echo "SQL: $sql";
		$this->db->exec($sql);

		// update cache values
		$this->updateCache();
	}
}