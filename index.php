<?php
// store the _POST data if we refreshed the page
if (isset($_POST['ACTION']) && $_POST['ACTION'] == 'Refresh') {
	// for a month
	$expire=time()+60*60*24*30;
	setcookie('POST', serialize($_POST), $expire);
}

?>

<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
	p {margin-left: 10px;}
	#err {color: red; font-weight: bold; margin: 10px;}
</style>

</head>
<body>
<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
$mem0 = memory_get_usage();

include_once 'data.php';
include_once 'plot.php';
include_once 'form.php';

date_default_timezone_set('Europe/Copenhagen');

// read CSV data file
$data = new data();

// echo '<pre>This is _POST:<br>';
// print_r($_POST);
// fake _POST with data from the POST cookie
if (count($_POST) == 0) {
// 	echo '_POST from COOKIE<br>';
	if (isset($_COOKIE['POST'])) {
		$_POST = unserialize($_COOKIE['POST']);
	}
} elseif (isset($_POST['ACTION']) && $_POST['ACTION'] == 'Upload') {
// 	echo '_POST ACTION changed from COOKIE<br>';

	// upload new CSV file if present
	if (isset($_FILES)) {
		if (isset($_FILES['CSVFILE'])) {
			if (($_FILES['CSVFILE']['error'] == 0) && ($_FILES['CSVFILE']['type'] == 'text/csv')) {
				$data->uploadCsvFile($_FILES["CSVFILE"]["tmp_name"], $_FILES["CSVFILE"]["name"]);
			}
		}
	}
	// get plot data from cookie
	if (isset($_COOKIE['POST'])) {
		unset($_POST['ACTION']);
		$_POST = array_merge(unserialize($_COOKIE['POST']));
	}
} elseif (isset($_POST['ACTION']) && $_POST['ACTION'] == 'Remove') {
	// we have remove data aset action
	$data->remove(array('hostname' => $_POST['hostname']));
	// get plot data from cookie
	if (isset($_COOKIE['POST'])) {
		unset($_POST['ACTION']);
		$_POST = array_merge(unserialize($_COOKIE['POST']));
	}
}

// set plot width and height
if (isset($_POST['plot-width'])) {
	if (is_numeric($_POST['plot-width'])) {
		$data->setPlotWidth($_POST['plot-width']);
	}
}
if (isset($_POST['plot-height'])) {
	if (is_numeric($_POST['plot-height'])) {
		$data->setPlotHeight($_POST['plot-height']);
	}
}

// echo 'This is mangled _POST:<br>';
// print_r($_POST);
// echo '<br>End of _POST</pre>';
// echo '<pre>This is _FILES:<br>';
// print_r($_FILES);
// echo '<br>End of _FILES</pre>';
// echo '<pre>This is _COOKIE:<br>';
// print_r($_COOKIE);
// echo '<br>End of _COOKIE</pre>';
// echo 'Header: '.implode(', ', $csv->getHeader()).'<br>';

echo '<h2>Speed tests presentation application</h2>';
echo '<p><i>This application can be used to provide graphical data presentation of the generated data by the <b>speed tests</b> collection.</i>';
echo ' Note that the tests are not executed by this application, but need to be ran before hand and generated results are then fed into this applicaiton.</p>';
echo '<p>More information about the usage, data set, test cases, etc. can be found <a href="#moreinfo">here</a>.</p>';
echo '<p><a href="'.$_SERVER['PHP_SELF'].'">HOME</a>';

echo '<hr>';
echo '<h2>Data Plots</h2>';

// generate form
echo getForm($data);

echo '<hr>';
// generate plots
echo plotData($data);

echo '<hr>';
echo '<div id=moreinfo></div>';
echo '<h2>More information</h2>';
echo '<h3>Test cases source code</h3>'
	.'<p><a href=speed_tests.tar.gz>Download</a>'
	.'<p>Uncompress and build using GNU GCC and GNU MAKE by issuing a single command: <i>make</i>.'
	.'<p>Run the preselected batch of tests cases by executing a BASH script: <i>bash run_tests.sh</i>.'
;
echo '<h3>GUI usage</h3>';
echo '<p>Start by adding at least one plot definition by clicking <i>Add</i> button.'
	.' Unlimited number of plots may be added.'
	.' Individual plots may also be removed by clicking on the <i>REM-x</i> button.'
	.'<p>Select desired filters from the dropdown menus. One of the filters MUST specify the X axis (select <i>x-axis</i>).'
	.' It is possible to plot the data of a specific filter by selecting <i>all</i> value from dropdown menu.'
	.' Average, max and total checkboxes select which data set is plotted. If none of the dropdown menus is set to <i>all</i> then multiple checkboxes may be selected, otherwise only single checkbox can be selected.'
	.'<p>After filters have been set or adjusted click on the <i>Refresh</i> button to generate plot(s).'
	.'<p>CSV file containing data set from a machine may be added by specifying CSV file and clicking on the <i>Upload</i> button.'
	.'<p>Data set previously uploaded can be removed by selecting hostname from a dropdown menu and clicking <i>Remove</i> button.'
	.'<p>NOTE: Machine information needs to be manually added after uploading new data set.'
	.'<p>Exclusion of one or more test cases is possible by providing test case name(s) (separated by ",") in the field (see <i>Exclude</i> row).'
;
echo '<h3>General</h3>';
echo '<p>Purpose of the tests was to demonstrate the required amount of time needed to process a certain amount of data samples.'
	.' Processing usually involved a simple (single) operation being performed that could appear in the real application.'
	.' The tests do not include time required to digitise the samples nor the sample buffer transport from the FPGA to the software.'
	.' Obtained results should not be taken as a given fact, but serve merly as a reference and an overview.'
	.' With the large enough data set (many different machines, many different compilers, many different compiler optimizations, many different tests,..) deciding if processing should be made in the software could be easier.'
	.'<p>Each test case was compiled with the predefined compiler switches.'
	.' Test case was then be executed with different set of command line arguments on the selected machine.'
	.' Each execution of the test case generated data that was stored in the CSV file named <i>tests.csv</i> (this is the file that this web application should be given to perform the presentation of the data).'
	.'<p>Test case command line switches may selected in a manner that resulting amount of processed data would correspond to ESS pulse length (~3 ms), repetition rate (14 Hz) and FPGA digitiser sampling at 100 MHz.'
	.' This would generate approximately 300000 samples per pulse. Additional assumptions were also tested (e.g. different data word sizes).'
;
echo $data->info();
echo '<h3>Test cases</h3>';
echo '<p>NOTE: Right side casts next to the assignment operator are determined by the <i>DTYPE2</i> value specified at compile time.'
	.' Value seen in the table below are from the actual test and are for reference only.'
	.'<p>NOTE2: <i>"(unsigned int)(2147483647 / 2)"</i> and <i>"(unsigned int)(2147483647 / 4)"</i> constructs are optimized away by the preprocessor (C macro).'
	.'<br>'
;
echo $data->knownTestOps();
echo '<h3>Machines</h3>';
echo $data->machine();

echo '<hr>';
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
$mem1 = memory_get_usage();
$mem = $mem1 - $mem0;
echo 'Page generated in '.$total_time.' seconds while using '.round($mem/1024/1024, 0).' MB of memory.<br>';
echo 'Release v1.4, 1 Sep 2014, <a href=mailto:hinko.kocevar@esss.se>Hinko Kočevar</a><br>';
?>

</body>
</html>
